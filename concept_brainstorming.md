Concept:

* Propose a weekly menu by default, taking into account the previous weeks, the last time the recipe was used, the season, the weekly schedule (i. e. which days are busy, etc.)

* Possibility to change each day, or substitute

* Generates shopping list automatically, for one or two shopping days

* Suggests prep and shortcuts to do on weekends, or on other days (for example, if chopping a bunch of onions monday and tuesday, chop double monday and freeze for tuesday)

* Include given ingredients

* Search for recipes by given ingredients sort by how matched (i. e.  include partial matches)

* Integration with calendar

* New recipe integration : Attempt to fill in database entry from photo, with option to validate and correct


Necessary info for each recipe:

    * ingredients
    * instructions
    * season
    * prep time 
    * cook time
    * date last cooked 
    * rating
    * notes
    * any prep steps possible (soak beans, cut onions, slow cooker the night before, etc.)
