FROM python:3

RUN pip install pytest

RUN pip install pillow pytesseract

RUN pip install opencv-python
RUN apt-get update
# RUN apt-get install -y mysql-server
RUN apt-get install -y tesseract-ocr
# RUN apt-get update \
#  && DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server